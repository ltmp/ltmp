#pragma once

struct Vector3f {

	float x;
	float y;
	float z;
	
	Vector3f();
	Vector3f(float x, float y, float z);
	Vector3f(Vector3f const& vec);
	Vector3f(Vector3f const* vec);

	Vector3f operator+(Vector3f const& val) const;
	Vector3f operator-(Vector3f const& val) const;
	Vector3f operator*(Vector3f const& val) const;
	Vector3f operator/(Vector3f const& val) const;

	Vector3f operator+(float val) const;
	Vector3f operator-(float val) const;
	Vector3f operator*(float val) const;
	Vector3f operator/(float val) const;

	/**
	 * Calcs linear interpolated vector
	 *
	 * @param start
	 * @param end
	 * @param time
	 * @return
	 */
	static Vector3f lerp(Vector3f const& start, Vector3f const& end, float time);

	/**
	 * Calcs Distance between two vectors.
	 *
	 * @param start
	 * @param end
	 * @return
	 */
	static float calcDistance(Vector3f const& start, Vector3f const& end);

	/**
	 * Calcs distance between two vectors. (Only on XY plane)
	 * @param start
	 * @param end
	 * @return
	 */
	static float calcXYDistance(Vector3f const& start, Vector3f const& end);

	Vector3f lerp(Vector3f const& end, float time) const;
	float calcDistance(Vector3f const& end) const;
	float calcXYDistance(Vector3f const& end) const;

	std::wstring toWString() const;
	std::string toString() const;

};