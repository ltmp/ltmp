#pragma once

struct Vector2f {

    float x;
    float y;

    Vector2f();
    Vector2f(float x, float y);
    Vector2f(Vector2f const& vec);
    explicit Vector2f(Vector2f const* vec);

    Vector2f operator+(Vector2f const& val) const;
    Vector2f operator-(Vector2f const& val) const;
    Vector2f operator*(Vector2f const& val) const;
    Vector2f operator/(Vector2f const& val) const;

    Vector2f operator+(float val) const;
    Vector2f operator-(float val) const;
    Vector2f operator*(float val) const;
    Vector2f operator/(float val) const;

    std::wstring toWString() const;
    std::string toString() const;

};