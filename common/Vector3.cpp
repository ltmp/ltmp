#include "common.h"

Vector3f::Vector3f()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
}

Vector3f::Vector3f(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3f::Vector3f(Vector3f const & vec)
{
	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;
}

Vector3f::Vector3f(Vector3f const* vec)
{
	this->x = vec->x;
	this->y = vec->y;
	this->z = vec->z;
}

Vector3f Vector3f::operator+(Vector3f const & val) const
{
	return { x + val.x, y + val.y, z + val.z };
}

Vector3f Vector3f::operator-(Vector3f const & val) const
{
	return { x - val.x, y - val.y, z - val.z };
}

Vector3f Vector3f::operator*(Vector3f const & val) const
{
	return { x * val.x, y * val.y, z * val.z };
}

Vector3f Vector3f::operator/(Vector3f const & val) const
{
	return { x / val.x, y / val.y, z / val.z };
}

Vector3f Vector3f::operator+(float val) const
{
	return { x + val, y + val, z + val };
}

Vector3f Vector3f::operator-(float val) const
{
	return { x - val, y - val, z - val };
}

Vector3f Vector3f::operator*(float val) const
{
	return { x * val, y * val, z * val };
}

Vector3f Vector3f::operator/(float val) const
{
	return { x / val, y / val, z / val };
}

Vector3f Vector3f::lerp(Vector3f const& start, Vector3f const& end, float time)
{
	return start + (end - start) * time;
}


float Vector3f::calcDistance(Vector3f const& start, Vector3f const& end)
{
	float x = start.x - end.x;
	float y = start.y - end.y;
	float z = start.z - end.z;

	return sqrtf(
			x * x + y * y + z * z
	);

}

float Vector3f::calcXYDistance(Vector3f const& start, Vector3f const& end)
{
	float x = start.x - end.x;
	float y = start.y - end.y;

	return sqrtf(
			x * x + y * y
	);

}

Vector3f Vector3f::lerp(Vector3f const& end, float time) const
{
	return Vector3f::lerp(Vector3f(this), end, time);
}

float Vector3f::calcDistance(Vector3f const& end) const
{
	return Vector3f::calcDistance(Vector3f(this), end);
}

float Vector3f::calcXYDistance(Vector3f const& end) const
{
	return Vector3f::calcXYDistance(Vector3f(this), end);
}

std::wstring Vector3f::toWString() const
{
	return L"{ " + std::to_wstring(this->x) + L" | " + std::to_wstring(this->y) + L" | " + std::to_wstring(this->z) + L" } ";
}

std::string Vector3f::toString() const
{
	return "{ " + std::to_string(this->x) + " | " + std::to_string(this->y) + " | " + std::to_string(this->z) + " } ";
}