#include "common.h"

Vector2f::Vector2f()
{
    this->x = 0;
    this->y = 0;
}

Vector2f::Vector2f(float x, float y)
{
    this->x = x;
    this->y = y;
}

Vector2f::Vector2f(Vector2f const & vec)
{
    this->x = vec.x;
    this->y = vec.y;
}

Vector2f::Vector2f(Vector2f const* vec)
{
    this->x = vec->x;
    this->y = vec->y;
}

Vector2f Vector2f::operator+(Vector2f const & val) const
{
    return { x + val.x, y + val.y };
}

Vector2f Vector2f::operator-(Vector2f const & val) const
{
    return { x - val.x, y - val.y };
}

Vector2f Vector2f::operator*(Vector2f const & val) const
{
    return { x * val.x, y * val.y };
}

Vector2f Vector2f::operator/(Vector2f const & val) const
{
    return { x / val.x, y / val.y };
}

Vector2f Vector2f::operator+(float val) const
{
    return { x + val, y + val };
}

Vector2f Vector2f::operator-(float val) const
{
    return { x - val, y - val };
}

Vector2f Vector2f::operator*(float val) const
{
    return { x * val, y * val };
}

Vector2f Vector2f::operator/(float val) const
{
    return { x / val, y / val };
}

std::wstring Vector2f::toWString() const
{
    return L"{ " + std::to_wstring(this->x) + L" | " + std::to_wstring(this->y)  + L" } ";
}

std::string Vector2f::toString() const
{
    return "{ " + std::to_string(this->x) + " | " + std::to_string(this->y) + " } ";
}