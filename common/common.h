#pragma once

#include <string>
#include <cmath>
#include <codecvt>
#include <locale>

#include "Vector2.h"
#include "Vector3.h"

#define LTMP_CURRENT_VERSION L"0.0.2"

//Define this is if you want to test sync locally/
//#define L5RP_LOCAL_SYNC_TEST

enum EntityType {
    ET_PLAYER
};

enum PedAction {
    PA_ROAMING,
    PA_UNKNOWN
};

typedef uint16_t dimensionId_t;

std::wstring stringToUnicode(const std::string& str);
std::string unicodeToString(const std::wstring& wstr);
